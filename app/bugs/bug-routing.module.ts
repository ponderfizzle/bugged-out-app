import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BugListComponent } from './bug-list/bug-list.component';

@NgModule ({
    imports: [ RouterModule.forChild([
        { path: '', redirectTo: 'index.html', pathMatch: 'full' },
        { path: 'index.html', component: BugListComponent },
        { path: '**', redirectTo: 'index.html' }
    ]) ],
    exports: [ RouterModule ]
})
export class BugRoutingModule { }