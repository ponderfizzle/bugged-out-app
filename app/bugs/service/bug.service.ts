import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { FirebaseConfigService } from '../../core/service/firebase-config.service'

import { Bug } from '../model/bug';

declare let swal: any; // Need this to get rid of errors in the IDE

@Injectable()

export class BugService {

    private bugsDbRef = this.fire.database.ref('/bugs');

    constructor(private fire: FirebaseConfigService) { }

    getAddedBugs(): Observable<any> {
        return Observable.create(obs => {
            this.bugsDbRef.on('child_added', bug => {
                const newBug = bug.val() as Bug;
                newBug.id = bug.key;
                obs.next(newBug);
            },
                err => {
                    obs.throw(err);
                });
        });
    }

    changedListener(): Observable<any> {
        return Observable.create(obs => {
            this.bugsDbRef.on('child_changed', bug => {
                const updatedBug = bug.val() as Bug;
                updatedBug.id = bug.key;
                obs.next(updatedBug);
            },
                err => {
                    obs.throw(err);
                });
        });
    }

    deletedListener(): Observable<any> {
        return Observable.create(obs => {
            this.bugsDbRef.on('child_removed', bug => {
                const deletedBug = bug.val() as Bug;
                deletedBug.id = bug.key;
                obs.next(deletedBug);
            },
                err => {
                    obs.throw(err);
                });
        });
    }

    addBug(bug: Bug) {
        const newBugRef = this.bugsDbRef.push();
        newBugRef.set({
            title: bug.title,
            status: bug.status,
            severity: bug.severity,
            description: bug.description,
            createdBy: bug.createdBy,
            createdDate: Date.now()
        })
            .catch(err => console.error("Unable to add bug to Firebase - ", err))
    }

    updateBug(bug: Bug) {
        const currentBugRef = this.bugsDbRef.child(bug.id);
        bug.id = null;
        bug.updatedBy = bug.updatedBy;
        bug.updatedDate = Date.now();
        currentBugRef.update(bug);
    }

    deleteBug(bug: Bug) {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to delete this bug record?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }).then(() => {
            this.bugsDbRef.child(bug.id).remove();
            swal(
                'Deleted!',
                'The bug record has been deleted.',
                'success',
            )
        },
            function (dismiss) {
                if (dismiss === 'cancel') {
                    swal(
                        'Cancelled',
                        'The bug record was not deleted',
                        'error'
                    )
                }
            }
            )
            .catch(swal.noop)
    }

}